#!/bin/bash
CRUMB=$(curl -s 'http://llovelock:89b324cb6c935cf9346d976fe9adab6f@localhost:8080/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,":",//crumb)')
curl -X POST http://localhost:8080/job/JUnit%20Demo/build \
      --user llovelock:89b324cb6c935cf9346d976fe9adab6f \
      -H $CRUMB \
      --data-urlencode json='{"parameter": [{"name":"PROJECTNAME", "value":"'$1'"}, {"name":"PROJECTID", "value":"'$2'"}]}'
echo DONE
